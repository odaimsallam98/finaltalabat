import mysql from 'mysql';

const db_config_1 = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'talabat',
    multipleStatements: true
}


export let connectionTalabat: mysql.Connection;

export function handleDisconnectTalabat(){
    connectionTalabat = mysql.createConnection(db_config_1);

    connectionTalabat.connect(function(err){
        if(err){
            console.log("Error When connecting to database ", err);
            setTimeout(handleDisconnectTalabat, 2000);
        }
    });

    
    connectionTalabat.on('error', function(err){
        console.log("db error ", err);

        if(err.code == 'PROTOCOL_CONNECTION_LOST'){
            console.log("Inside the if err.code");
            handleDisconnectTalabat();
        }
        else{
            console.log("error connecting to database throw error");
            throw err;
        }
    });
}

handleDisconnectTalabat();

