import express, { json } from 'express';
import morgan from 'morgan';
import fs from 'fs';
import path from 'path';
import {connectionTalabat, handleDisconnectTalabat} from './dbconnectTalabat';
import bodyparser from 'body-parser';
import cors from 'cors';
import { User } from './user';
import { Restaurant } from './restaurant';
import { MenuItem } from './MenuItem';

const port:number = 3000;
let app = express();

app.use(cors());
app.use(morgan('common',{
    stream: fs.createWriteStream(path.join(__dirname, '../access.log'), {flags: 'a'})
}));
app.use('/static', express.static(path.join(__dirname, 'public')));
//app.use('/', express.static(path.join(__dirname, 'public')));
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());

app.get('/', (req, res)=>{
    res.send("This is the index page");
});



//getUserFlags
app.get('/getUserFlags',(req,res)=>{
    console.log("getflags");
    connectionTalabat.query(`SELECT * FROM flags`, (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            console.log(result);
            res.json(result);
        }
    })
});

//edit a User Flags
app.put('/editUserFlags/:admin/:loggedIn/:defaultUser', (req, res)=>{
    console.log("set flags");
    let admin=req.params['UserPassword'];
    let loggedIn=req.params['loggedIn'];
    let defaultUser=req.params['defaultUser'];
    connectionTalabat.query(`UPDATE flags SET admin=${admin},loggedIn=${loggedIn},defaultUser=${defaultUser}`, (err, result)=>{
        if(err){
            res.status(404).json({"Error": err});
            console.log(err);
        }
        else{
            console.log("");
            res.json({'Success': result});
        }
    })
});



//getDefaultUser
app.get('/getDefaultUser',(req,res)=>{

    let default1:string = "default";
    let id:number = 2;
    connectionTalabat.query(`SELECT * FROM user WHERE name='${default1}' AND password='${default1}' AND id=${id} `, (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            res.json(result);
        }
    })
});

//check user login 
app.get('/LogIn/:UserName/:UserPassword',(req,res)=>{
    let UserName = req.params['UserName'];
    let UserPassword = req.params['UserPassword'];
    connectionTalabat.query(`
    SELECT CASE WHEN EXISTS (
        SELECT *
        FROM user
        WHERE name='${UserName}' AND password='${UserPassword}'
    )
    THEN TRUE
    ELSE FALSE END AS bool`, (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            //console.log(`${JSON.parse(result[0])}`);
            console.log(result);
            res.send(result);
        }
    })
});

//get user from users table
app.get('/getUser/:UserName/:UserPassword',(req,res)=>{
    let UserName = req.params['UserName'];
    let UserPassword = req.params['UserPassword'];

    connectionTalabat.query(`SELECT * FROM user WHERE name='${UserName}' AND password='${UserPassword}' `, (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            res.json(result);
        }
    })
});

//register/insert a user
app.post('/RegisterUser', (req, res)=>{
    console.log("from register user in back end ")
    let user: User = req.body.user;
    connectionTalabat.query(`
    INSERT INTO user (name, password) VALUES
    ('${user.name}', '${user.password}');`
    , (err, result)=>{
        if(err){
            console.log('Error '+ err);
            res.json({'Error': err});
        }
        else{
            res.json({'Created': result});
        }
    });
});

//get all restaurants
app.get('/RestaurantsList', /*auth,*/ (req, res)=>{
    console.log("inside the get '/RestaurantsList' route");
    connectionTalabat.query('SELECT * FROM restaurants', (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            res.json(result);
        }
    })
});

//add a new restaurant
app.post('/AddRestaurant', (req, res)=>{
    console.log("add restaurant");
    let rest: Restaurant = req.body.rest;
    connectionTalabat.query(`
    INSERT INTO restaurants (name, city, street, latitude, longitude, image) VALUES
    ('${rest.name}', '${rest.city}', '${rest.street}', ${rest.latitude}, ${rest.longitude}, '${rest.image}');
    `, (err, result)=>{
        if(err){
            console.log('Error '+ err);
            res.json({'Error': err});
        }
        else{
            res.json({'Created': result});
        }
    });
});

//edit a restaurant
app.put('/editRestaurants/:id', (req, res)=>{
    console.log("from put in backend hiiiiiiiiii");
    let id = req.params['id'];
    let rest: Restaurant = req.body.rest;

    console.log("from put in backend hiiiiiiiiii");
    connectionTalabat.query(`UPDATE restaurants SET name='${rest.name}',city='${rest.city}',street='${rest.street}',latitude=${rest.latitude},longitude=${rest.longitude},image='${rest.image}'  WHERE id=${id}`, (err, result)=>{
        if(err){
            res.status(404).json({"Error": err});
            console.log(err);
        }
        else{
            console.log("from put in backend hiiiiiiiiii");
            res.json({'Success': result});
        }
    })
});

//delete a restaurant
app.delete('/deleteRestaurant/:id', (req, res)=>{
    
    let ResID = req.params['id'];
    
    connectionTalabat.query(`
    DELETE FROM restaurants WHERE id=${ResID};
    DELETE FROM menuitems WHERE rest_id=${ResID}; 
    DELETE FROM usersorders WHERE ResID=${ResID}; 
    `,(err, result)=>{
        if(err){
            console.log(err);
            res.status(404).json({"Error": err});
        }
        else{
            res.json({"success": result});
        }
    });

});

//get menu items for a restaurant
app.get('/MenuItems/:resid', /*auth,*/ (req, res)=>{
    console.log("from back end to get menu items of a restaurant");
    let id = req.params['resid'];
    connectionTalabat.query(`SELECT * FROM menuitems WHERE rest_id=${id}`, (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            res.json(result);
        }
    })
});

//add menu item 
app.post('/addMenuItem', (req, res)=>{
    let MI: MenuItem = req.body.menuItem;
    connectionTalabat.query(`
    INSERT INTO menuitems (name, price, descr, image, rest_id) VALUES
    ('${MI.name}', ${MI.price}, '${MI.descr}', '${MI.image}', ${MI.rest_id});
    `, (err, result)=>{
        if(err){
            console.log('Error '+ err);
            res.json({'Error': err});
        }
        else{
            res.json({'Created': result});
        }
    });
});


//edit menu item
app.put('/editMenuItem/:ResID/:MenuItemID', (req, res)=>{
    console.log("from put in backend hiiiiiiiiii");
    
    let ResID = req.params['ResID'];
    let MenuItemID = req.params['MenuItemID'];
    let MI: MenuItem = req.body.MI;

    console.log("from put in backend hiiiiiiiiii");
    connectionTalabat.query(`UPDATE menuitems SET name='${MI.name}',price=${MI.price},descr='${MI.descr}',image='${MI.image}',rate=${MI.rate} WHERE id=${MenuItemID} AND rest_id=${ResID}`, (err, result)=>{
        if(err){
            res.status(404).json({"Error": err});
            console.log(err);
        }
        else{
            console.log("from put in backend hiiiiiiiiii");
            res.json({'Success': result});
        }
    })
});

//delete menu item 
app.delete('/deleteMenuItem/:ResID/:MenuItemID', (req, res)=>{
    console.log("delete menu item");
    let ResID = req.params['ResID'];
    let MenuItemID = req.params['MenuItemID'];
    connectionTalabat.query(`
    DELETE FROM menuitems WHERE rest_id=${ResID} AND id=${MenuItemID};
    DELETE FROM usersorders WHERE ResID=${ResID} AND MenuItemID=${MenuItemID}
    `,(err, result)=>{
        if(err){
            console.log(err);
            res.status(404).json({"Error": err});
        }
        else{
            res.json({"success": result});
        }
    })
});


//check if order for a specific user exists
app.get('/checkMenuItem/:UserID/:ResID/:MenuItemID',(req,res)=>{
    let UserID = req.params['UserID'];
    let ResID = req.params['ResID'];
    let MenuItemID = req.params['MenuItemID'];
    console.log("check order");
    connectionTalabat.query(`
    
    SELECT CASE WHEN EXISTS (
        SELECT *
        FROM usersorders
        WHERE UserID=${UserID} AND ResID=${ResID} AND MenuItemID=${MenuItemID}
    )
    THEN TRUE
    ELSE FALSE END AS bool`, (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            console.log(result);
            res.send(result);
        }
    })
});

//new order 
app.post('/orderMenuItem/:UserID/:ResID/:MenuItemID', (req, res)=>{
    
    let UserID = req.params['UserID'];
    let ResID = req.params['ResID'];
    let MenuItemID = req.params['MenuItemID'];
    
    let rest: Restaurant = req.body.rest;
    connectionTalabat.query(`
    INSERT INTO usersorders (UserID, ResID, MenuItemID) VALUES
    (${UserID} , ${ResID} ,${MenuItemID});
    `, (err, result)=>{
        if(err){
            console.log('Error '+ err);
            res.json({'Error': err});
        }
        else{
            res.json({'Created': result});
        }
    });
});

//delete order
app.delete('/deleteOrderMenuItem/:UserID/:ResID/:MenuItemID', (req, res)=>{
    
    let UserID = req.params['UserID'];
    let ResID = req.params['ResID'];
    let MenuItemID = req.params['MenuItemID'];
    
    connectionTalabat.query(`DELETE FROM usersorders WHERE UserID=${UserID} AND ResID=${ResID} AND MenuItemID=${MenuItemID}`,(err, result)=>{
        if(err){
            console.log(err);
            res.status(404).json({"Error": err});
        }
        else{
            res.json({"success": result});
        }
    })
});

//get Orders Of a Restaurant For A User
app.get('/ListResOrdersMenuItems/:UserID/:ResID',(req,res)=>{
    let UserID = req.params['UserID'];
    let ResID = req.params['ResID'];
    
    connectionTalabat.query(`    
    SELECT * FROM menuitems 
    JOIN usersorders  ON menuitems.rest_id=usersorders.ResID 
    WHERE usersorders.UserID=${UserID} AND usersorders.ResID=${ResID}`, (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            res.json(result);
        }
    })
});

//getAllOredersForAllUsers
app.get('/ListAllOrdersMenuItems',(req,res)=>{
    
     console.log("from sow all orders");
    connectionTalabat.query(`
    SELECT * FROM menuitems 
    JOIN usersorders ON menuitems.rest_id=usersorders.ResID`, (err, result)=>{
        if(err){
            console.log("query error: " + err);
            res.json({"Error": err});
        }
        else{
            console.log(result);
            res.json(result);
        }
    })
});

app.listen(port, ()=>{
    console.log("Server is listening on port " + port);
});
