"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuItem = void 0;
class MenuItem {
    constructor(name, price, descr, image, id, rest_id, rate, ordered) {
        this.name = name;
        this.price = price;
        this.descr = descr;
        this.image = image;
        this.id = id;
        this.rest_id = rest_id;
        this.rate = rate;
        this.ordered = ordered;
    }
}
exports.MenuItem = MenuItem;
//# sourceMappingURL=MenuItem.js.map