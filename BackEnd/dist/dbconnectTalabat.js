"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleDisconnectTalabat = exports.connectionTalabat = void 0;
const mysql_1 = __importDefault(require("mysql"));
const db_config_1 = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'talabat',
    multipleStatements: true
};
function handleDisconnectTalabat() {
    exports.connectionTalabat = mysql_1.default.createConnection(db_config_1);
    exports.connectionTalabat.connect(function (err) {
        if (err) {
            console.log("Error When connecting to database ", err);
            setTimeout(handleDisconnectTalabat, 2000);
        }
    });
    exports.connectionTalabat.on('error', function (err) {
        console.log("db error ", err);
        if (err.code == 'PROTOCOL_CONNECTION_LOST') {
            console.log("Inside the if err.code");
            handleDisconnectTalabat();
        }
        else {
            console.log("error connecting to database throw error");
            throw err;
        }
    });
}
exports.handleDisconnectTalabat = handleDisconnectTalabat;
handleDisconnectTalabat();
//# sourceMappingURL=dbconnectTalabat.js.map