"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Restaurant = void 0;
class Restaurant {
    constructor(name, city, street, latitude, longitude, image, id, rate) {
        this.name = name;
        this.city = city;
        this.street = street;
        this.latitude = latitude;
        this.longitude = longitude;
        this.image = image;
        this.id = id;
        this.rate = rate;
    }
}
exports.Restaurant = Restaurant;
//# sourceMappingURL=restaurant.js.map