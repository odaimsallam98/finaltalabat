import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from './services/data.service';
import { RestaurantsDataService } from './services/restaurants-data.service';
import { UserDataService } from './services/user-data.service';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'talabar-project';
  constructor(
    private userService:UserDataService,
    private _route: ActivatedRoute,
    private _router: Router,
    private restaurantDataService: RestaurantsDataService,
    private dataService:DataService) {
  }

  navigateToResList(){
    this._router.navigateByUrl(`list_restaurants`);
  }

}
