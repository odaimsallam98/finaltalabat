import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem } from '../models/MenuItem';
import { DataService } from '../services/data.service';
import { UserDataService } from '../services/user-data.service';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css']
})
export class MenuItemComponent implements OnInit {

  @Input() menuItem:MenuItem;
   ordered:boolean = false;
   //orderedTest:boolean = false;
  errorMessage:string="";
  responseText:string="";
  
  @Output() newItemEvent = new EventEmitter();

  admin:boolean = this.userService.admin; 
  loggedIn:boolean = this.userService.loggedIn;
  defaultUser:boolean = this.userService.defaultUser;

  constructor(
    private dataService:DataService,
    private userService:UserDataService,
    private _router: Router) { }

  ngOnInit(): void {
    this.dataService.checkMenuItemOredered(this.userService.user.id,this.menuItem.rest_id,this.menuItem.id).subscribe(
      (data)=> {
        if(data[0].bool)
        {
          console.log(data[0].bool);
          this.ordered = true;
        }
        else{
          console.log(data[0].bool);
          this.ordered = false;
        }
      },
      (error)=> this.responseText = error
    );
    
  }
  order(){
    let responseText = "";
    this.dataService.OrderMenuItem(this.userService.user.id,this.menuItem.rest_id,this.menuItem.id).subscribe(
      (data)=> {
        this.ordered = true;
        this.newItemEvent.emit();
      },
      (error)=> responseText = error
    );
  }
  deleteOrder(){
    let responseText = "";
    this.dataService.deleteOrderMenuItem(this.userService.user.id,this.menuItem.rest_id,this.menuItem.id).subscribe(
      (data)=> {
        this.ordered = false;
        this.newItemEvent.emit();
      },
      (error)=> responseText = error
    );
  }
  editMenuItem(){
    console.log("navigation to menu item with res id "+this.menuItem.rest_id);
    this._router.navigateByUrl(`edit_menu_item/${this.menuItem.rest_id}/${this.menuItem.id}`);
  }
  deleteMenuItem(){
    this.dataService.deleteMenuItem(this.menuItem.rest_id,this.menuItem.id).subscribe(
      (data)=> {
        this.newItemEvent.emit();
      },
      (error)=> this.responseText = error
    );
  }

}
