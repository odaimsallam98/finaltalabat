import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MenuItem } from '../models/MenuItem';
import { MenuItemsDataService } from '../services/menu-items-data.service';
import { UserDataService } from '../services/user-data.service';

@Component({
  selector: 'app-list-orders',
  templateUrl: './list-orders.component.html',
  styleUrls: ['./list-orders.component.css']
})
export class ListOrdersComponent implements OnInit {

  admin:boolean = this.userService.admin; 
  loggedIn:boolean = this.userService.loggedIn;
  defaultUser:boolean = this.userService.defaultUser;


  _orders : MenuItem[]=[];
  _id:number;
  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private menuItemsService:MenuItemsDataService,
    private userService:UserDataService,
    ){}

    ngOnInit(): void {

      this.route.params.subscribe(
          (params: Params) => {this._id = +params['resid'];}
        );
        this._orders=this.menuItemsService.getRestOrderedMenuItems(this._id);
    }
    orderMenuItem(id: number) {
      //this.ordersListService.addOrder(newOrder);
      this.menuItemsService.order(this.userService.user.id,this._id,id);
    }
  
    deleteOrderMenuItem(id: number) {
      this.menuItemsService.deleteOrder(this.userService.user.id,this._id,id);
    }
    checkOrdered(id:number):boolean
    {
      return this.menuItemsService.checkOrdered(this.userService.user.id,this._id,id);
    }
    goTOMain(){
      this._router.navigateByUrl(`list_restaurants`);
    }

    backToMenu() {
    this._router.navigateByUrl(`list_menu_items/${this._id}`)
    }

}
