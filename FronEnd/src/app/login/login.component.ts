import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { NgForm } from '@angular/forms';
import { FormsModule }   from '@angular/forms';
import { UserDataService } from '../services/user-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router,private userService:UserDataService) { }

  ngOnInit() {
  }

  submit(f){
    if (f.valid){
      console.log("value " + f.value.username)
      this.userService.logIn(f.value.username,f.value.password);
    }
  }

}
