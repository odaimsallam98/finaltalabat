import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem } from '../models/MenuItem';
import { Restaurant } from '../models/restaurant';
import { MenuItemsDataService } from '../services/menu-items-data.service';
import { RestaurantsDataService } from '../services/restaurants-data.service';
import { UserDataService } from '../services/user-data.service';

@Component({
  selector: 'app-list-ordered-menu-items',
  templateUrl: './list-ordered-menu-items.component.html',
  styleUrls: ['./list-ordered-menu-items.component.css']
})
export class ListOrderedMenuItemsComponent implements OnInit {

  
  admin:boolean = this.userService.admin; 
  loggedIn:boolean = this.userService.loggedIn;
  defaultUser:boolean = this.userService.defaultUser;

  _restaurants: Restaurant[] = []
  //_orders: MenuItem[] = [];
  constructor(
    private menuItemsService: MenuItemsDataService,
    private restaurantsListService: RestaurantsDataService,
    private route: ActivatedRoute,
    private _router: Router,
    private userService:UserDataService,
  ) { }

  ngOnInit(): void {
    
    this._restaurants = this.restaurantsListService.restaurants;
  }
  orderMenuItem(i:MenuItem){
    this.menuItemsService.order(this.userService.user.id,i.rest_id,i.id);  }
  deleteOrder(i:MenuItem) {
    this.menuItemsService.deleteOrder(this.userService.user.id,i.rest_id,i.id);
  }
  checkOrdered(resId:number,id:number):boolean
  {
    return this.menuItemsService.checkOrdered(this.userService.user.id,resId,id);
  }

  getResOrders(res: Restaurant): MenuItem[]{
    return this.menuItemsService.getRestOrderedMenuItems(res.id);
  }
  goTOMain() {
    this._router.navigateByUrl(`list_restaurants`);

  }

}
