import { Injectable } from '@angular/core';
import { element } from 'protractor';

import { MenuItem } from '../models/MenuItem';
import { DataService } from './data.service';
import { UserDataService } from './user-data.service';
@Injectable({
  providedIn: 'root'
})
export class MenuItemsDataService {
  menuItems :MenuItem[]=[];
  errorMessage="";
  constructor(private dataService:DataService,private userService:UserDataService) { }
  
  getMenuItemsOfRest(resId:number):MenuItem[]{
    let MIList:MenuItem[]=[];
    let responseText = "";
    this.dataService.getMenuItems(resId).subscribe(
      (data:MenuItem[])=> {
        MIList = data;
      },
      (error)=> responseText = error
    );
    return MIList;
  }
  addMenuItem(menuItem:MenuItem){
    let responseText = "";
    this.dataService.addMenuItem(menuItem).subscribe(
      (data)=> responseText = JSON.stringify(data),
      (error)=> responseText = error
    );
  }
  editMenuItem(menuItem:MenuItem){
    let responseText = "";
    this.dataService.editMenuItem(menuItem).subscribe(
      (data)=> responseText = JSON.stringify(data),
      (error)=> responseText = error
    );
  }
  deleteMenuItem(resID:number,id:number){
    let responseText = "";
    this.dataService.deleteMenuItem(resID,id).subscribe(
      (data)=> responseText = JSON.stringify(data),
      (error)=> responseText = error
    );
  }
  checkOrdered(UserID:number,ResID:number,MenuItemID:number){
    let ordered:boolean = false;
    let responseText = "";
    this.dataService.checkMenuItemOredered(UserID,ResID,MenuItemID).subscribe(
      (data)=> {
        if(data[0].bool)
        {
          ordered = true;
        }
      },
      (error)=> responseText = error
    );
    return ordered;
  }
  order(UserID:number,ResID:number,MenuItemID:number){
    let responseText = "";
    this.dataService.OrderMenuItem(UserID,ResID,MenuItemID).subscribe(
      (data)=> responseText = JSON.stringify(data),
      (error)=> responseText = error
    );
  }
  deleteOrder(UserID:number,ResID:number,MenuItemID:number){
    let responseText = "";
    this.dataService.deleteOrderMenuItem(UserID,ResID,MenuItemID).subscribe(
      (data)=> responseText = JSON.stringify(data),
      (error)=> responseText = error
    );
  }
  getAllOrderedMenuItems():MenuItem[]{
    let responseText="";
    let _menuItems:MenuItem[]=[];
    this.dataService.getAllOrders().subscribe(
      (data:MenuItem[])=> {
        _menuItems = data;
      },
      (error)=> responseText = error
    );
    return _menuItems;
  }
  getRestOrderedMenuItems(resId:number):MenuItem[]{
    let responseText="";
    let _menuItems:MenuItem[]=[];
    this.dataService.getOrdersOfRes(this.userService.user.id,resId).subscribe(
      (data:MenuItem[])=> {
        _menuItems = data;
      },
      (error)=> responseText = error
    );    
    return _menuItems;
  }
}
//