import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { DataService } from './data.service';
import { NavBarComponent } from '../nav-bar/nav-bar.component';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  admin:boolean = false; 
  loggedIn:boolean = false;
  defaultUser:boolean = true;
  user:User = new User('default','default',2);

  array:boolean[]=[this.admin,this.loggedIn,this.defaultUser];
  
  getAdmin():boolean{
    return this.admin;
  }

  errorMessage:string ="";
  constructor( private dataService:DataService,private _router:Router){ }
  
  ifAdmin(id:number,):boolean{
    if(id==1)
    {
      return true;
    }
    else return false;
  }
  
  /*
  alterNavBar(admin:boolean,loggedIn:boolean,defaultUser:boolean,user:User){
    this.navbar.admin=admin;
    this.navbar.defaultUser=defaultUser;
    this.navbar.loggedIn=loggedIn;
    this.navbar.user=this.user;
  }
  */

  setLoggedIn(user:User){
    this.defaultUser=false;
    if(this.ifAdmin(user.id))
    {
      this.admin = true; 
      this.loggedIn = true;
    //  this.alterNavBar(this.admin,this.loggedIn,this.defaultUser,this.user);
      console.log("admin in");
    }else{
      this.admin = false; 
      this.loggedIn = true;
     // this.alterNavBar(this.admin,this.loggedIn,this.defaultUser,this.user);
      console.log("user in");
    }
    this.user =user;
  }
  registerUser(user:User){
    let responseText = "";
        this.dataService.register(user).subscribe(
      (data)=> responseText = JSON.stringify(data),
      (error)=> responseText = error
    )
  }
  logIn(name:string,password:string){
    let responseText = "";
        this.dataService.logIn(name,password).subscribe(
        (data:boolean)=> {
          if(data[0].bool)
          {
            this.dataService.getUser(name,password).subscribe(
              (user:User)=> {
                let newUser:User = new User(user[0].name,user[0].password,user[0].id);
                this.setLoggedIn(newUser);
                this._router.navigateByUrl(`list_restaurants`);
              },
              (error)=> responseText = error
            );       
          }else{
            this._router.navigateByUrl(`RegisterUser`);
          }
        },
        (error)=> responseText = error
    )
  }
}
