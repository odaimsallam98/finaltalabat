import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { Restaurant } from '../models/restaurant';
import { CATCH_ERROR_VAR } from '@angular/compiler/src/output/output_ast';
import { Order } from '../models/Order';
import { MenuItem } from '../models/MenuItem';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  loginURL: string="http://localhost:3000/LogIn";
  registerURL: string="http://localhost:3000/RegisterUser";
  getDefaultUserURL: string="http://localhost:3000/getDefaultUser";
  getUserURL: string="http://localhost:3000/getUser";
  
  resURL: string="http://localhost:3000/RestaurantsList";
  addRestURL: string="http://localhost:3000/AddRestaurant";
  editResURL: string="http://localhost:3000/editRestaurants";
  deleteResURL:string="http://localhost:3000/deleteRestaurant";
  
  menuItemsURl: string="http://localhost:3000/MenuItems";
  addMIURL:string="http://localhost:3000/addMenuItem";
  editMIURL:string="http://localhost:3000/editMenuItem";
  deleteMIURL:string="http://localhost:3000/deleteMenuItem";
  
  checkMenuItem: string="http://localhost:3000/checkMenuItem";
  orderMenuItem:string="http://localhost:3000/orderMenuItem";
  deleteOrderMI:string="http://localhost:3000/deleteOrderMenuItem";
  getOrdersofResURL="http://localhost:3000/ListResOrdersMenuItems";
  getAllOrdersURL="http://localhost:3000/ListAllOrdersMenuItems";
  
  constructor(private http: HttpClient) {
   }

   //deal with users

   //get the default user when there is no user logged in 
   getDefaultUser(): Observable<User>{
    return this.http.get<User>(`${this.getDefaultUserURL}`);
   }
   //check if the user logged in exists
   logIn(name:string,password:string): Observable<boolean>
   {
    return this.http.get<boolean>(`${this.loginURL}/${name}/${password}`);
   }

   getUser(name:string,password:string): Observable<User>
   {
    return this.http.get<User>(`${this.getUserURL}/${name}/${password}`);
   }

   //register a user to the database
   register(user:User):Observable<Object>{
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    let body = {
      "user":user
    }
    console.log("from data service ")
   return this.http.post<Object>(this.registerURL, body, httpOptions);
   }

   //deal with restaurants 
  getRestaurants(): Observable<Restaurant[]>{
    return this.http.get<Restaurant[]>(this.resURL);
  }
  addRestaurant(rest: Restaurant): Observable<Object>{
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    let body = {
      "rest":rest
    }
   return this.http.post(this.addRestURL, body, httpOptions);
  }
  editRestaurant(rest: Restaurant): Observable<void>{
    console.log("from edit restaurant in angular1");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    console.log("from edit restaurant in angular2");
    let body = {
      "rest":rest
    }
    console.log("from edit restaurant in angular3 "+rest.id);
   return this.http.put<void>(`${this.editResURL}/${rest.id}`, body, httpOptions);
  }
  deleteRestaurant(resID:number): Observable<Object>
  {
    return this.http.delete(`${this.deleteResURL}/${resID}`,);
  }

  //deal with MenuItems
  getMenuItems(resId:number): Observable<Object>{
    return this.http.get(`${this.menuItemsURl}/${resId}`);
  }
  addMenuItem(MI:MenuItem): Observable<Object>{
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    let body = {
      "menuItem":MI
    }
   return this.http.post(this.addMIURL, body, httpOptions);
  }
  editMenuItem(MI:MenuItem): Observable<Object>{
    console.log("from edit menu item in angular1");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    console.log("from edit restaurant in angular2");
    let body = {
      "MI":MI
    }
    console.log("from edit restaurant in angular3 "+MI.id + MI.rest_id + MI.name);
   return this.http.put(`${this.editMIURL}/${MI.rest_id}/${MI.id}`, body, httpOptions);
  }
  
  deleteMenuItem(resID:number,menuItemId:number): Observable<Object>{
    return this.http.delete(`${this.deleteMIURL}/${resID}/${menuItemId}`,);
  }
  
  //deal with Orders
  checkMenuItemOredered(UserID:number,ResID:number,MenuItemID:number):Observable<boolean>
  {
    return this.http.get<boolean>(`${this.checkMenuItem}/${UserID}/${ResID}/${MenuItemID}`);     
  }
  OrderMenuItem(UserID:number,ResID:number,MenuItemID:number):Observable<Object>{
    let order:Order =  new Order(UserID,ResID,MenuItemID);
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    let body = {
      "order":order
    }
   return this.http.post(`${this.orderMenuItem}/${UserID}/${ResID}/${MenuItemID}`, body, httpOptions);
  }
  deleteOrderMenuItem(UserID:number,ResID:number,MenuItemID:number):Observable<Object>{
  return this.http.delete(`${this.deleteOrderMI}/${UserID}/${ResID}/${MenuItemID}`)
  }
  getOrdersOfRes(UserID:number,ResID:number): Observable<Object>{
    return this.http.get<MenuItem[]>(`${this.getOrdersofResURL}/${UserID}/${ResID}`);
  }
  getAllOrders(): Observable<Object>{
    return this.http.get<MenuItem[]>(this.getAllOrdersURL);
  }

}
