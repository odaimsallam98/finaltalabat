import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Restaurant } from '../models/restaurant';
import { Observable } from 'rxjs';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class RestaurantsDataService {
  
  errorMessage: String='error odai';
  restaurants: Restaurant[];

  constructor(private http: HttpClient,private dataService:DataService) { }
  getRestaurants():Restaurant[]
  {
    return this.restaurants;
  }
  
  setRestaurants(res: Restaurant[]){
    this.restaurants = res;
  }


  deleteRestaurant(resID:number){}

  
  addRestaurant(restaurant: Restaurant) {
    let responseText = "";
    this.dataService.addRestaurant(restaurant).subscribe(
      (data)=> responseText = JSON.stringify(data),
      (error)=> responseText = error
    )
  }
  
  setRestaurant(res:Restaurant):void{
   let responseText = "";
    this.dataService.editRestaurant(res).subscribe(
      (data)=> responseText = JSON.stringify(data),
      (error)=> responseText = error
    )
  }

  rateRest(rating:number, id:number)
  {
    for(let i=0;i<this.restaurants.length;i++)
    {
      if(this.restaurants[i].id == id)
      {
        this.restaurants[i].rate = rating;
      }
    }
  }
}
