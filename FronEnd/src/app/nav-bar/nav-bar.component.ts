import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../models/user';
import { DataService } from '../services/data.service';
import { RestaurantsDataService } from '../services/restaurants-data.service';
import { UserDataService } from '../services/user-data.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  admin:boolean = this.userService.admin; 
  loggedIn:boolean = this.userService.loggedIn;
  defaultUser:boolean = this.userService.defaultUser;
  
  user:User= this.userService.user;

  constructor(
    private userService:UserDataService,
    private _route: ActivatedRoute,
    private _router: Router,
    private restaurantDataService: RestaurantsDataService,
    private dataService:DataService) {
  }

  ngOnInit(): void {
  }

  navigateToResList(){
    this._router.navigateByUrl(`list_restaurants`);
  }
  navigateToAddRes(){
    this._router.navigateByUrl(`add_restaurant`);
  }
  navigateToAllOrders(){
    this._router.navigateByUrl(`list_ordered_menu_items`);
  }
  navigateToLogIn(){
    this._router.navigateByUrl(`LogIn`);
  }
  navigateToRegister(){
    this._router.navigateByUrl(`RegisterUser`);
  }
}
