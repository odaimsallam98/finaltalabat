import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { DataService } from '../services/data.service';
import { UserDataService } from '../services/user-data.service';
import { NgForm } from '@angular/forms';
import { FormsModule }   from '@angular/forms';



@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {

  user: User = new User();

  constructor(private dataService: DataService, private _router: Router,private userService:UserDataService,) { }

  ngOnInit() {
  }

  submit(f){
    if (f.valid){
      console.log("from valid in angular");
      this.userService.registerUser(new User(f.value.username,f.value.password));
      this._router.navigateByUrl(`LogIn`);
  }

}
}