import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { from } from 'rxjs';
import { Restaurant } from '../models/restaurant';
import { ActivatedRoute, Router } from '@angular/router';
import { RestaurantsDataService } from '../services/restaurants-data.service';
import { DataService } from '../services/data.service';
import { UserDataService } from '../services/user-data.service';

@Component({
  selector: 'app-list-all-restarants',
  templateUrl: './list-all-restarants.component.html',
  styleUrls: ['./list-all-restarants.component.css']
})
export class ListAllRestarantsComponent implements OnInit {
  
  admin:boolean = this.userService.admin; 
  loggedIn:boolean = this.userService.loggedIn;
  defaultUser:boolean = this.userService.defaultUser;
  
  rating:number;
  restaurants:Restaurant[]=[];
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private restaurantDataService: RestaurantsDataService,
    private dataService:DataService,
    private userService:UserDataService
    ) {
  }
  errorMessage:string = "";

  ngOnInit(): void {
    
    //this.restaurantDataService.getRestaurantsFromServer();
    //this.restaurants = this.restaurantDataService.getRestaurants();
    
    this.dataService.getRestaurants().subscribe(
      (res: Restaurant[])=> {
        this.restaurants = res;
      },
      (error: any)=> {
        console.log(error);
        this.errorMessage = error;
      }
    );
    
    //this.userService.getDefaultUser1();
    //this.restaurants = this.restaurantDataService.restaurants;
    //this.restaurantDataService.setRestaurants(this.restaurants);
  }
  
  updateRestaurant(id:number) {
    this._router.navigateByUrl(`/edit_restaurant/${id}`);
  }
  ShowMenu(id:number){
    this._router.navigateByUrl(`list_menu_items/${id}`);
  }
  showOrders(res_id:number)
  {
    this._router.navigateByUrl(`list_orders/${res_id}`);
  }
  deleteRest(res_id:number){
    let responseText:string="";
    this.dataService.deleteRestaurant(res_id).subscribe(
      (data)=> {
        this.dataService.getRestaurants().subscribe(
          (res: Restaurant[])=> {
            this.restaurants = res;
          },
          (error: any)=> {
            console.log(error);
            this.errorMessage = error;
          }
        );
      },
      (error)=> responseText = error
    );
    //this._router.navigate(['/list_restaurants']);

  }
}


