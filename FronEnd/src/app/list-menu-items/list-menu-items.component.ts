import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params,Router } from '@angular/router';
import { MenuItem } from '../models/MenuItem';
import { DataService } from '../services/data.service';
import { MenuItemsDataService } from '../services/menu-items-data.service';
import { UserDataService } from '../services/user-data.service';


@Component({
  selector: 'app-list-menu-items',
  templateUrl: './list-menu-items.component.html',
  styleUrls: ['./list-menu-items.component.css']
})
export class ListMenuItemsComponent implements OnInit {

  
  admin:boolean = this.userService.admin; 
  loggedIn:boolean = this.userService.loggedIn;
  defaultUser:boolean = this.userService.defaultUser;

  _menu_items : MenuItem[]=[];
  _id:number;
 
  errorMessage:string = "";
constructor(
  private route: ActivatedRoute,
  private _router: Router,
  private menuItemsService : MenuItemsDataService,
  private dataService:DataService,
  private userService:UserDataService
  ){}

  ngOnInit(): void {
    this.route.params.subscribe(
        (params: Params) => {this._id = +params['resid'];}
      );
      this.dataService.getMenuItems(this._id).subscribe(
        (MI: MenuItem[])=> {this._menu_items=MI},
        (error: any)=> {
          console.log(error);
          this.errorMessage = error;
        }
      );
  }

  refreshList(){
    this.dataService.getMenuItems(this._id).subscribe(
      (MI: MenuItem[])=> {this._menu_items=MI},
      (error: any)=> {
        console.log(error);
        this.errorMessage = error;
      }
    );
  }

  editMenuItem(id:number){
    console.log("navigation to menu item with res id "+this._id);
    this._router.navigateByUrl(`edit_menu_item/${this._id}/${id}`);
  }

  addMenuItem(){
    this._router.navigateByUrl(`/add_item/${this._id}`);
    console.log("navigation to menu item with res id "+this._id);
  }
  deleteMenuItem(id:number){
    let responseText = "";
    this.dataService.deleteMenuItem(this._id,id).subscribe(
      (data)=> {
        this.dataService.getMenuItems(this._id).subscribe(
          (MI: MenuItem[])=> {this._menu_items=MI},
          (error: any)=> {
            console.log(error);
            this.errorMessage = error;
          }
        );
      },
      (error)=> responseText = error
    );
  }


  order(MenuItemID:number){
    let responseText = "";
    this.dataService.OrderMenuItem(this.userService.user.id,this._id,MenuItemID).subscribe(
      (data)=> {
        this.dataService.getMenuItems(this._id).subscribe(
          (MI: MenuItem[])=> {this._menu_items=MI},
          (error: any)=> {
            console.log(error);
            this.errorMessage = error;
          }
        );
      },
      (error)=> responseText = error
    );
  }

  deleteOrder(MenuItemID:number){
    let responseText = "";
    this.dataService.deleteOrderMenuItem(this.userService.user.id,this._id,MenuItemID).subscribe(
      (data)=> {
        this.dataService.getMenuItems(this._id).subscribe(
          (MI: MenuItem[])=> {this._menu_items=MI},
          (error: any)=> {
            console.log(error);
            this.errorMessage = error;
          }
        );
      },
      (error)=> responseText = error
    );
  }




/*
  orderMenuItem(id: number) {
    //this.ordersListService.addOrder(newOrder);
    this.menuItemsService.order(this.userService.user.id,this._id,id);
  }

  deleteOrderMenuItem(id: number) {
    this.menuItemsService.deleteOrder(this.userService.user.id,this._id,id);
  }
  
  checkOrdered(id:number):boolean
  {
    return this.menuItemsService.checkOrdered(this.userService.user.id,this._id,id);
  }
  */

 checkOrdered(MenuItemID:number){
  let ordered:boolean = false;
  let responseText = "";
  console.log("check order");
  this.dataService.checkMenuItemOredered(this.userService.user.id,this._id,MenuItemID).subscribe(
    (data)=> {
      if(data[0].bool)
      {
        ordered = true;
      }
    },
    (error)=> responseText = error
  );
  return ordered;
}
  showOrders() {
    this._router.navigateByUrl(`list_orders/${this._id}`)
  }

}
