import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MenuItemsDataService } from '../services/menu-items-data.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { MenuItem } from '../models/MenuItem';
import { UserDataService } from '../services/user-data.service';
import { User } from '../models/user';

@Component({
  selector: 'app-add-menu-item-form',
  templateUrl: './add-menu-item-form.component.html',
  styleUrls: ['./add-menu-item-form.component.css']
})

export class AddMenuItemFormComponent implements OnInit {

 _id : number;
 
  user:User = this.userService.user;
  admin:boolean = this.userService.admin; 
  loggedIn:boolean = this.userService.loggedIn;
  defaultUser:boolean = this.userService.defaultUser;

  constructor(private MenuItemsService:MenuItemsDataService, private route:ActivatedRoute, private _router: Router,private userService:UserDataService,) { }

  ngOnInit(): void {
    this.route.params.subscribe(
        (params: Params) => {this._id = +params['resid'];}
      );
  }

  addMenuItem(f:NgForm)
  {
    
    this.MenuItemsService.addMenuItem(
      new MenuItem(
        f.value.MIName,
        f.value.price,
        f.value.description,
        f.value.image,
        this._id,10,false));
    this._router.navigateByUrl(`list_menu_items/${this._id}`);
  }
}